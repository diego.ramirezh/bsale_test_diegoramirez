{{-- se extiende la plantilla master --}}
@extends('layout.master')

{{-- titulo --}}
@section('title')
{{$categoria->name}}
@endsection
{{-- fin titulo --}}

{{-- estilos de esta pagina --}}
@section('css')
<style>
    .card, .card-footer{
        background: white;
    }
    .card-img-top{
        width: 100%;
        height: 220px;
        object-fit: cover;
    }
    .pagination {
        justify-content: center;
    }
</style>
@endsection
{{-- fin de estilos --}}


{{-- contenido --}}
@section('content')
    <div class="container">
        <div class="row py-4">
            <div class="col-lg-3 m-3 m-lg-0">
                <div class="row  p-3 border border-gray rounded" style="background: white">
                    {{-- nombre de la categoria --}}
                    <div class="col-12">
                        <h3>{{$categoria->name}}</h3>
                        <hr>
                    </div>
                    {{-- fin del nombre de categoria --}}

                    {{-- filtros de listado de productos --}}
                    <div class="col-12">
                        <h5>Ordenar por:</h5>
                        <form action="{{route('categorias.ordenar',['categoria'=>$categoria->name])}}">
                            @csrf
                            <select class="custom-select" id="ordenar" name="ordenar">
                                <option disabled selected hidden>Seleccione</option>
                                <option value="">ninguno</option>
                                <option value="1">Precio mas bajo</option>
                                <option value="2">Precio mas alto</option>
                                <option value="3">Mejor oferta</option>
                                <option value="4">Nombre (A - Z)</option>
                                <option value="5">Nombre (Z - A)</option>
                            </select>
                            <button type="submit" class="btn btn-sm btn-success mt-2">Filtrar</button>
                        </form>
                    </div>
                    {{-- fin de filtros --}}

                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">

                    {{-- listado de productos --}}
                    @foreach ($productos as $producto)
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                            <img class="card-img-top" src=" {{$producto->url_image}}" onerror="this.src='{{asset('img/no-image.jpg')}}'"  alt="product">
                            <div class="card-body">
                                <div class="card-title">
                                    <h5> {{$producto->name}}</h5>
                                   
                                </div>
                                @if($producto->discount == 0)
                                <hr>
                                <div>
                                    <h5 >{{$producto->price}}</h5>
                                </div>
                                @else
                                <hr>
                                <strike style="color:red">{{$producto->price}}</strike> <h5 class="float-right"> -{{$producto->discount}}%</h5><br>
                                <h5 style="color:green">{{$producto->precio_rebajado}}</h5>
                                @endif
                            </div>
                            <div class="card-footer">
                                <a href="{{route('producto.show',['producto'=>$producto->id])}}">
                                    <button class="btn btn-success w-100">Detalles</button>
                                </a>
                            </div>
                            </div>
                        </div>
                    @endforeach
                    {{-- fin de listado de productos --}}
                    
                    {{-- paginacion --}}
                    <div class="col-12">
                        {{ $productos->links() }}
                    </div>
                    {{-- fin de paginacion --}}
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- fin contenido --}}