@extends('layout.master')

@section('title')
{{$busqueda}}
@endsection

@section('css')
<style>
    .pagination {
        justify-content: center;
    }
</style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col m-4 p-4" style="background: white">
                {{-- encabezado --}}
                <div class="row">
                    <div class="col mb-3">
                        <h2>Resultados de la búsqueda</h2>
                        <b>Palabras clave:</b> <span> {{$busqueda}}</span>
                    </div>
                </div>
                {{-- fin de encabezado --}}


                {{-- inicio de if  --}}

                {{-- revisa si se encontraron resultados de busqueda --}}
                @if($productos->isEmpty())
                <div class="row">
                    <div class="col">
                        <h4 class="text-secondary">No se han encontrado resultados</h4>
                    </div>
                </div>
                @else
                <div class="row">
                    {{-- listado de productos --}}
                    @foreach ($productos as $producto)
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="card h-100">
                            {{-- imagen --}}
                            <a href="#"><img class="card-img-top" src=" {{$producto->url_image}}" onerror="this.src='{{asset('img/no-image.jpg')}}'"  alt=""></a>
                            {{-- fin imagen --}}

                            {{-- card body --}}
                            <div class="card-body">
                                {{-- nombre del producto --}}
                                <div class="card-title">
                                    <h5> {{$producto->name}}</h5>
                                </div>
                                {{-- fin nombre del producto --}}

                                {{-- inicio de if --}}

                                {{-- si el producto no tiene descuento --}}
                                @if($producto->discount == 0)
                                    <hr>
                                    <div>
                                        {{-- lista el precio sin descuento --}}
                                        <h5 >{{$producto->price}}</h5>
                                    </div>
                                {{-- si el producto tiene descuento --}}
                                @else
                                    <hr>
                                    {{-- lista el descuento junto a su precio rebajado --}}
                                    <strike style="color:red">{{$producto->price}}</strike> <h5 class="float-right"> -{{$producto->discount}}%</h5><br>
                                    <h5 style="color:green">{{$producto->precio_rebajado}}</h5>
                                @endif
                                {{-- fin de if --}}
                            </div>
                            {{-- fin card body --}}

                            {{-- card footer --}}
                            <div class="card-footer">
                                <button class="btn btn-success w-100">Detalle</button>
                            </div>
                            {{-- fin card footer --}}
                        </div>
                    </div>
                    @endforeach
                    {{-- fin de listado de productos --}}

                    {{-- paginacion --}}
                    <div class="col-12">
                        {{ $productos->links() }}
                    </div>
                    {{-- fin paginacion --}}
                </div>
                @endif
                {{-- fin de if --}}
            </div>
        </div>
    </div>
@endsection