@extends('layout.master')
@section('title') Home @endsection

@section('content')
    {{-- contenido de la pagina --}}
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="my-4"> ¡Bienvenido a la tienda!</h2>
                <hr>
            </div>
            <div class="col-12">
                {{-- carousel --}}
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel" data-slide-to="1"></li>
                        <li data-target="#carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="https://revistagestion.ec/sites/default/files/2019-04/encabezado%20pag%20web%20%289%29_0.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://www.mexicoescultura.com/galerias/actividades/principal/pox__bebida_sagrada_900.png" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://www.kerry.com/-/media/images/blog/beer-hero.ashx" alt="Third slide">
                        </div>
                    </div>
                        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <hr>
                </div>
                {{--     --}}
            <div class="row">
                <div class="col my-3">
                    <h5>Categorias</h5>
                </div>
            </div>
            <div class="row">
                @foreach ($categorias as $categoria)
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="card h-100">
                            <div class="card-body">
                            <h4 class="card-title text-center">
                                <a style="color:black" href="{{route('categorias.index',$categoria->name)}}">{{$categoria->name}}</a>
                            </h4>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            </div>
        </div>
    </div>
        <footer class="py-3 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Prueba para <span style="color:orange">bsale</span> hecha por <b>Diego Ramirez</b></p>
            </div>
        </footer>
@endsection