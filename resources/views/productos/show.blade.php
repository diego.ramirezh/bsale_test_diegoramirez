{{-- extiende de la plantilla master --}}
@extends('layout.master')

{{-- titulo navegador --}}
@section('title')
    {{$producto->name}}
@endsection
{{-- fin titulo --}}

{{-- contenido --}}
@section('content')
    <div class="container p-3">
        {{-- nombre del producto --}}
        <div class="row m-2">
            <div class="col-12">
                <h3>{{$producto->name}}</h3>
            </div>
        </div>
        {{-- fin nombre --}}

        {{-- contenido de la pagina --}}
        <div class="row mb-2">
            <div class="col-md-6">
                <div class="card mb-2">

                    {{-- imagen del producto --}}
                    <div class="card-body text-center">
                        <img class="w-100 h-100" style="object-fit:cover" src="{{$producto->url_image}}" alt="">
                    </div>
                    {{-- fin imagen del producto --}}

                </div>
            </div>
            <div class="col-md-6 mb-2">
                <div class="card h-100">
                    <div class="card-body">
                        {{-- precio y compra  --}}
                        <div class="row h-100 m-1">
                            <div class="col-12">
                                descripcion de envio.. etc
                            </div>
                            <div class="col-12 align-self-end">
                                {{-- revisa si hay descuento --}}

                                {{-- si el producto no tiene descuento --}}
                                @if($producto->discount == 0)
                                    <div>
                                        <h5 >{{$producto->price}}</h5>
                                    </div>
                                {{-- ================================== --}}
                                
                                {{-- si el producto tiene descuento --}}
                                @else
                                    <strike style="color:red">{{$producto->price}}</strike> <h5 class="float-right"> -{{$producto->discount}}%</h5><br>
                                    <h5 style="color:green">{{$producto->precio_rebajado}}</h5>
                                @endif
                                {{-- ================================== --}}
                                <hr>
                                {{-- boton de comprar producto --}}
                                <button type="button" class="btn btn-success w-100">Comprar</button>
                                {{-- fin de boton --}}
                            </div>
                        </div>
                        {{-- fin precio y compra --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                {{-- descripcion del producto --}}
                <div class="card">
                    <div class="card-header">
                        <h5>Descripción del producto</h5>
                    </div>
                    <div class="card-body">
                        <span>No hay descripción porque no hay tabla de descripción de producto, pero puede agregarse!.</span>
                    </div>
                </div>
                {{-- fin descripcion --}}
            </div>
        </div>
        {{-- fin contenido de la pagina --}}
    </div>
@endsection