<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href=" {{ asset('css/main.css')}} ">
    @yield('css')
    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
    <title>@yield('title') - bsale Test</title>
</head>
<body>
    {{-- navbar --}}
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark p-2">
        
        {{-- navbar brand --}}
        <a class="navbar-brand mx-4" style="color:orange" href="/">bsale Test</a>
        {{-- fin navbar brand --}}

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            {{-- navegacion --}}
            <ul class="navbar-nav mr-auto">
                
                <li class="nav-item active">
                    <a class="nav-link" href="/">Inicio</a>
                </li>
                {{-- categorias --}}
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categorias
                    </a>
                    <div class="dropdown-menu" id="categorias-dropdown" aria-labelledby="navbarDropdown"></div>
                </li>
                {{-- fin categorias --}}
            </ul>
            {{-- fin navegacion --}}

            {{-- busqueda de productos --}}
            <div>
                <form action="{{route('busqueda')}}" autocomplete="off" class="form-inline">
                    @csrf
                    <div class="form-group">
                        <div class="dropdown">
                            <input class="form-control mr-sm-2" type="search" id="busqueda" name="busqueda" placeholder="¿Qué estás buscando?" aria-label="Busqueda" style="width: 500px">
                            <div class="dropdown-menu" id="dropdown-search" style="width: 500px"></div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">buscar</button>
                </form>
            </div>
            {{-- fin busqueda de productos --}}

        </div>
    </nav>
    {{-- fin navbar --}}
    @yield('content')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src=" {{asset('js/bootstrap.min.js') }} "></script>

    {{-- Llenar categoria --}}
    <script>
        $("#categorias-dropdown").html('<b class="dropdown-item">cargando...</b>');
        $.get( "/api/categorias", function( data ) {
            // handle success
            var html='';
            var elemento = document.getElementById('categorias-dropdown');
            data.forEach((category) => {
                html += '<a class="dropdown-item" href="/categorias/'+category.name+'">'+category.name+'</a>'
            })
            $("#categorias-dropdown").html(html);
        })
        .fail(function() {
            alert( "error" );
        });
    </script>
    {{-- fin llenar categoria --}}

    {{-- Busqueda --}}
    <script>
        $("#busqueda").keyup(function() {
            var texto = $(this).val();
            if(texto.length == 0){
                document.getElementById('dropdown-search').classList.remove('show');
            }else{
                document.getElementById('dropdown-search').classList.add('show');  
            }
            $.ajax({
                    url: "/api/busqueda",
                    data: {texto: texto},
                    beforeSend: function() {
                        document.getElementById('dropdown-search').innerHTML = "<span>cargando...</span>";
                    },
                    success: function(data) {
                        var response = JSON.parse(data)
                        if (response.response_html != "")
                        {
                            document.getElementById('dropdown-search').innerHTML = response.response_html;
                        }else{
                            document.getElementById('dropdown-search').innerHTML = "no se encontraron resultados";
                        }
                    },
                    error: function(err) {
                        alert(err);

                    },
                })
        });
    </script>
    {{-- fin de busqueda --}}

    {{-- extender scripts en caso de necesitar --}}
    @yield('scripts')
    {{-- fin --}}
</body>
</html>