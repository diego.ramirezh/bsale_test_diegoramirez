<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("
        INSERT INTO `category` (`id`, `name`) VALUES
            (1, 'bebida energetica'),
            (2, 'pisco'),
            (3, 'ron'),
            (4, 'bebida'),
            (5, 'snack'),
            (6, 'cerveza'),
            (7, 'vodka');
        ");
    }
}
