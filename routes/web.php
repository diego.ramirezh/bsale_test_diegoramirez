<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;


Route::get('/',[HomeController::class, 'index'])->name('home');

// categorias
Route::get('/categorias/{categoria}' ,[CategoryController::class,'index'])->name('categorias.index');
Route::get('/categorias/{categoria}/ordenar',[CategoryController::class,'ordenar'])->name('categorias.ordenar');

// producto
Route::get('/producto/{producto}',[ProductController::class,'show'])->name('producto.show');
Route::get('/productos/busqueda',[HomeController::class,'busqueda'])->name('busqueda');