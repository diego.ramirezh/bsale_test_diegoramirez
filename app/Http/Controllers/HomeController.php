<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;


class HomeController extends Controller
{
    /**
     * Pagina home inicio
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        //se retornan todas las categorias
        $categorias = Category::all();
        return view('home',compact('categorias'));
    }

    /**
     * Obtener todas las categorias en la base de datos
     *
     * @return Category
     */
    public function categories(){
        //se usa query builder de laravel para obtener todas las categorias en la bd
        $categorias = Category::all();
        return $categorias;
    }

    /**
     * Busqueda de producto en BD
     *
     * @return \Illuminate\Http\Response
     */
    public function busqueda(Request $request){
        // usa bootstrap como paginacion
        Paginator::useBootstrap();
        
        $busqueda = $request->busqueda; 
        $productos = Product::where('name','LIKE','%'.$busqueda.'%')->paginate(6);

        # calcular y guardar precios descontados
        foreach($productos as $producto){
            $precio = $producto->price;
            $descuento = 100 - $producto->discount;

            $resultado = $precio * ($descuento/100);
            $producto->precio_rebajado =  "$ ".number_format($resultado, 0, ',', '.'). " CLP"; 

            // formatear numeros
            $producto->price = "$ ".number_format($producto->price, 0, ',', '.'). " CLP";
        }

        return view('categorias.busqueda',compact('busqueda','productos'));
    }
}
