<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;


class ProductController extends Controller
{
    /**
     * Muestra el producto solicitado
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($producto)
    {
        $producto = Product::find($producto); 

        # calcular y guardar precios descontado
       
        $precio = $producto->price;
        $descuento = 100 - $producto->discount;

        $resultado = $precio * ($descuento/100);
        $producto->precio_rebajado =  "$ ".number_format($resultado, 0, ',', '.'). " CLP"; 

        // formatear numeros
        $producto->price = "$ ".number_format($producto->price, 0, ',', '.'). " CLP";
        

        return view('productos.show',compact(['producto']));
    }
}
