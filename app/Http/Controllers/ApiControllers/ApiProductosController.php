<?php

namespace App\Http\Controllers\ApiControllers;

use App\Models\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiProductosController extends Controller
{
    //

    //
    public function busqueda(Request $request)
    {
        //verifica que sea ajax 
        if($request->ajax())
        {
            //se obtiene el texto que se quiere buscar
            $busqueda = $request->get('texto');

            // si la busqueda es vacia, se retorna null
            if($busqueda != '')
            {
                // se busca en la base de datos las coincidencias con LIKE 
                $productos = Product::where('name','LIKE','%'.$busqueda.'%')->paginate(6);
            }else{
                $productos = null;
            }

            // el html que será retornado
            $response_html = "";
            
            if ($productos != null)
            {
                //recorre los productos retornados por la sentencia sql (LIKE)
                foreach($productos as $producto)
                {
                    // por cada iteracion se generará un html con ruta y el nombre del producto
                    $response_html .='<a class="dropdown-item" href="'.route('producto.show',['producto'=>$producto->id]).'">'.$producto->name.'</a>
                    ';
                }
            }
            
            //se guardan los datos
            $data = array(
                'response_html' => $response_html
            );

            //retorna la respuesta ajax como json
            echo json_encode($data);
        }
    }
}
