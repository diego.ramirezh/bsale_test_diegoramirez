<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;


class CategoryController extends Controller
{
    /**
     * Muestra la categoria con sus productos
     *
     * @return \Illuminate\Http\Response
     */
    public function index($categoria) // la categoria que se busca
    {
        // usa bootstrap como paginacion
        Paginator::useBootstrap();

        // se extraen los datos
        $categoria = Category::where('name',$categoria)->first(); // se extrae la categoria buscandola por el nombre
        $productos = Product::where('category',$categoria->id)->paginate(6);

        # calcular y guardar precios descontados
        foreach($productos as $producto){
            $precio = $producto->price;
            $descuento = 100 - $producto->discount;

            $resultado = $precio * ($descuento/100);
            $producto->precio_rebajado =  "$ ".number_format($resultado, 0, ',', '.'). " CLP"; 

            // formatear numeros
            $producto->price = "$ ".number_format($producto->price, 0, ',', '.'). " CLP";
        }
        
        return view('categorias.index',compact('productos','categoria'));
    }

     /**
     * Ordena segun la opción seleccionada
     *
     * @return \Illuminate\Http\Response
     */
    public function ordenar(Request $request, $categoria){
        
        $categoria = Category::where('name',$categoria)->first(); // se extrae la categoria buscandola por el nombre

        // usa bootstrap como paginacion
        Paginator::useBootstrap();

        // se extraen los datos
        switch($request->ordenar){
            case 1: //ordenar por precio mas bajo
                $productos = Product::where('category',$categoria->id)->orderBy('price', 'asc')->paginate(6);
                break;
            case 2: //ordenar por precio mas alto
                $productos = Product::where('category',$categoria->id)->orderBy('price', 'desc')->paginate(6);
                break;
            case 3: //ordenar por mejor oferta
                $productos = Product::where('category',$categoria->id)->orderBy('discount', 'desc')->paginate(6);
                break;
            case 4: //ordenar por nombre ascendente
                $productos = Product::where('category',$categoria->id)->orderBy('name', 'asc')->paginate(6);
                break;
            case 5: //ordenar por nombre descendente
                $productos = Product::where('category',$categoria->id)->orderBy('name', 'desc')->paginate(6);
                break;
            default:
                $productos = Product::where('category',$categoria->id)->paginate(6);
        }

        # calcular y guardar precios descontados
        foreach($productos as $producto){
            $precio = $producto->price;
            $descuento = 100 - $producto->discount;

            $resultado = $precio * ($descuento/100);
            $producto->precio_rebajado =  "$ ".number_format($resultado, 0, ',', '.'). " CLP"; 

            // formatear numeros
            $producto->price = "$ ".number_format($producto->price, 0, ',', '.'). " CLP";
        }

        return view('categorias.index',compact('productos','categoria'));
    }
}
